@ECHO OFF

IF EXIST *.env (GOTO BEGIN_ARTISAN) ELSE (GOTO BEGIN_CREATE_PROJECT)

:BEGIN_CREATE_PROJECT
ECHO -------------------------------------------
SET /p projectname="Enter Project Name:"
call laravel new %projectname%
GOTO END_CONSOLE


:BEGIN_ARTISAN
ECHO -------------------------------------------


ECHO [1] laravel new
ECHO [2] php artisan make:auth
ECHO [3] php artisan make:controller
ECHO [4] php artisan migrate:refresh --seed
ECHO [5] php artisan make:seeder
ECHO [6] php artisan db:seed
ECHO [7] exit

CHOICE /N /C:1234567 /M "Select Artisan Options:"%1
IF ERRORLEVEL ==7 GOTO 7
IF ERRORLEVEL ==6 GOTO 6
IF ERRORLEVEL ==5 GOTO 5
IF ERRORLEVEL ==4 GOTO 4
IF ERRORLEVEL ==3 GOTO 3
IF ERRORLEVEL ==2 GOTO 2
IF ERRORLEVEL ==1 GOTO 1
GOTO END_ARTISAN

SET /p ArtisanOpt= "Select Artisan Options:"

:1
	SET /p projectname="Enter Project Name:"
	call laravel new %projectname%
GOTO BEGIN_ARTISAN

:2
	call composer require laravel/ui
	call php artisan ui bootstrap --auth
	call npm install
	call npm run dev
GOTO BEGIN_ARTISAN

:3
	SET /p ControllerName="Enter Controller Name:"
	call php artisan make:controller %ControllerName%Controller
GOTO BEGIN_ARTISAN


:4
	call php artisan migrate:refresh --seed
GOTO BEGIN_ARTISAN

:5
	SET /p SeederName="Enter Seeder Name:"
	call php artisan make:seeder %SeederName%Seeder
GOTO BEGIN_ARTISAN

:6
	call composer dump-autoload
	call php artisan db:seed
GOTO BEGIN_ARTISAN

:7
GOTO END_CONSOLE

:END_CONSOLE


PAUSE